import {performDiff} from "../src/index";

describe('function performDiff', () => {

  it('simple insert', () => {
    const str1 = '123';
    const str2 = '13';

    const result = {
      operation: 'insert',
      start: 1,
      length: 1,
      content: '2'
    }

    const actualResult = performDiff(str1, str2);

    expect(actualResult).toContainEqual(result)
  })

  it('simple delete', () => {
    const str1 = '13';
    const str2 = '123';

    const result = {
      operation: 'delete',
      start: 1,
      length: 1,
      content: '2'
    }

    const actualResult = performDiff(str1, str2);

    expect(actualResult).toContainEqual(result)
  })

  it('simple replace', () => {
    const str1 = '153';
    const str2 = '123';

    const result1 = {
      operation: 'insert',
      start: 1,
      length: 1,
      content: '5'
    };

    const result2 = {
      operation: 'delete',
      start: 1,
      length: 1,
      content: '2'
    };

    const actualResult = performDiff(str1, str2);

    expect(actualResult).toEqual(expect.arrayContaining([result1, result2]));
  })

  it('empty params', () => {
    // const str1 = '13';
    // const str2 = '123';
    //
    // const result = {
    //   operation: 'delete',
    //   start: 1,
    //   length: 1,
    //   content: '2'
    // }
    //
    // const actualResult = performDiff(str1, str2);

    expect(performDiff('', '2')).toContainEqual({
      operation: 'delete',
      start: 0,
      length: 1,
      content: '2'
    })

    expect(performDiff('0', '')).toContainEqual({
      operation: 'insert',
      start: 0,
      length: 1,
      content: '0'
    })

    expect(performDiff('', '')).toEqual([])
  })

  it('identical contents', () => {
    expect(performDiff('123456', '123456')).toEqual([])
  })

})
