import {revertDiff, DiffOperation, DiffResultElement} from "../src";

describe('function revertDiff', () => {


  it('single insert', () => {
    const str1 = '123';
    const str2 = '13';
    let diff: DiffResultElement[] = [
      {
        operation: DiffOperation.INSERT,
        start: 1,
        length: 1,
        content: '2'
      }
    ];
    expect(revertDiff(str1, diff)).toBe(str2);
  });
  it('double insert', () => {
    const str1 = '12234456';
    const str2 = '123456';
    let diff: DiffResultElement[] = [
      {
        operation: DiffOperation.INSERT,
        start: 2,
        length: 1,
        content: '2'
      },
      {
        operation: DiffOperation.INSERT,
        start: 5,
        length: 1,
        content: '4'
      }
    ];
    expect(revertDiff(str1, diff)).toBe(str2);
  });
  it('single delete', () => {
    const str1 = '13';
    const str2 = '123';
    let diff: DiffResultElement[] = [
      {
        operation: DiffOperation.DELETE,
        start: 1,
        length: 1,
        content: '2'
      }
    ];
    expect(revertDiff(str1, diff)).toBe(str2);
  });
  it('double delete', () => {
    const str1 = '13';
    const str2 = '1234';
    let diff: DiffResultElement[] = [
      {
        operation: DiffOperation.DELETE,
        start: 1,
        length: 1,
        content: '2'
      },
      {
        operation: DiffOperation.DELETE,
        start: 2,
        length: 1,
        content: '4'
      }
    ];
    expect(revertDiff(str1, diff)).toBe(str2);
  });
  it('single replace', () => {
    const str1 = '143';
    const str2 = '123';
    let diff: DiffResultElement[] = [
      {
        operation: DiffOperation.DELETE,
        start: 1,
        length: 1,
        content: '2'
      },
      {
        operation: DiffOperation.INSERT,
        start: 1,
        length: 1,
        content: '4'
      }
    ]
    expect(revertDiff(str1, diff)).toBe(str2);
  })

  // it('buggy case', () => {
  //   const stringToRevert = '<p>12<img src="123" width="123" height="123" alt="">3<img src="12" width="12" height="12" alt=""></p>';
  //   const expected = '<p>12<img src="123" width="123" height="123" alt="">3</p>' // old string
  //   let diff: DiffResultElement[] = [
  //     {operation: DiffOperation.INSERT, content: "12", start: 3, length: 2},
  //     {operation: DiffOperation.DELETE, content: "b", start: 6, length: 1},
  //     {operation: DiffOperation.INSERT, content: "img s", start: 6, length: 5},
  //     {operation: DiffOperation.INSERT, content: "c=\"123\"", start: 12, length: 7},
  //     {operation: DiffOperation.INSERT, content: "wi", start: 20, length: 2},
  //     {operation: DiffOperation.INSERT, content: "th=\"123\" height=\"123\" ", start: 23, length: 22},
  //     {operation: DiffOperation.INSERT, content: "l", start: 46, length: 1},
  //     {operation: DiffOperation.DELETE, content: "a-", start: 48, length: 2},
  //     {operation: DiffOperation.INSERT, content: "=\"\">3<i", start: 48, length: 7},
  //     {operation: DiffOperation.INSERT, content: "g sr", start: 56, length: 4},
  //     {operation: DiffOperation.INSERT, content: "=\"12\" width=\"12\" h", start: 61, length: 18},
  //     {operation: DiffOperation.DELETE, content: "-bo", start: 80, length: 3},
  //     {operation: DiffOperation.INSERT, content: "i", start: 80, length: 1},
  //     {operation: DiffOperation.DELETE, content: "us", start: 82, length: 2},
  //     {operation: DiffOperation.INSERT, content: "ht", start: 82, length: 2},
  //     {operation: DiffOperation.INSERT, content: "2\" alt=\"", start: 87, length: 8},
  //   ]
  //
  //   expect(revertDiff(stringToRevert, diff)).toBe(expected);
  // })
})
