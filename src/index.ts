import * as diff from 'fast-diff';

/**
 * @typedef {string} DiffOperation
 **/

/**
 * @enum {DiffOperation}
 */
enum DiffOperation {
  INSERT = 'insert',
  DELETE = 'delete'
}

/**
 * @typedef {Object} DiffResultElement
 * @property {DiffOperation} operation
 * @property {string} content
 * @property {number} start
 * @property {number} length
 */
interface DiffResultElement {
  operation: DiffOperation,
  content: string,
  start: number,
  length: number,
}


/**
 * Finds difference between two strings and returns difference that can be applied to the oldString to re-create newString
 * @param {string} newString
 * @param {string} oldString
 * @returns {object:{}}
 */
function performDiff(newString: string, oldString: string): DiffResultElement[] {
  let diffResult: diff.Diff[] = diff(oldString, newString);
  // console.log(diffResult);
  let result: DiffResultElement[] = [];
  let currentIndex: number = 0;
  for (let i = 0; i < diffResult.length; i++) {
    let element: diff.Diff = diffResult[i];
    switch (element[0]) {
      case -1:
        //delete
        result.push({
          operation: DiffOperation.DELETE,
          content: element[1],
          start: currentIndex,
          length: element[1].length,
        });
        break;
      case 0:
        //similar
        currentIndex += element[1].length;
        break;
      case 1:
        //insert
        result.push({
          operation: DiffOperation.INSERT,
          content: element[1],
          start: currentIndex,
          length: element[1].length,
        });
        currentIndex += element[1].length;
        break;
      default:
        console.error('unexpected diff result:', element);
        break;
    }
  }
  return result;
}

/**
 *
 * @param string
 * @param diffInfo
 */
function applyDiff(string: string, diffInfo: DiffResultElement[]) {
  let diff: DiffResultElement[] = [...diffInfo];
  let result: string = string;

  while (diff.length) {
    // Important note - apply diff changes should be done in the same order as they are defined in an array.
    // while revert should be done in reverse order
    const element: DiffResultElement = diff.shift();

    switch (element.operation) {
      case DiffOperation.DELETE:
        result = result.substr(0, element.start) + result.substr(element.start + element.length);
        break;

      case DiffOperation.INSERT:
        result = result.substr(0, element.start) + element.content + result.substr(element.start);
        break;
    }
  }
  return result;
}


function revertDiff(string: string, diffInfo: DiffResultElement[]) {
  let diff: DiffResultElement[] = [...diffInfo];
  let result: string = string;

  while (diff.length) {
    // See note in applyDiff
    const element: DiffResultElement = diff.pop();

    switch (element.operation) {
      case DiffOperation.INSERT:
        result = result.substr(0, element.start) + result.substr(element.start + element.length);
        break;

      case DiffOperation.DELETE:
        result = result.substr(0, element.start) + element.content + result.substr(element.start);
        break;
    }
  }
  return result;
}

export {
  DiffOperation,
  DiffResultElement,
  performDiff,
  applyDiff,
  revertDiff
}
