"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
  for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
  for (var r = Array(s), k = 0, i = 0; i < il; i++)
    for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
      r[k] = a[j];
  return r;
};
Object.defineProperty(exports, "__esModule", {value: true});
var diff = require("fast-diff");
var DiffOperation;
(function (DiffOperation) {
  DiffOperation["INSERT"] = "insert";
  DiffOperation["DELETE"] = "delete";
})(DiffOperation || (DiffOperation = {}));
exports.DiffOperation = DiffOperation;
function performDiff(newString, oldString) {
  var diffResult = diff(oldString, newString);
  var result = [];
  var currentIndex = 0;
  for (var i = 0; i < diffResult.length; i++) {
    var element = diffResult[i];
    switch (element[0]) {
      case -1:
        result.push({
          operation: DiffOperation.DELETE,
          content: element[1],
          start: currentIndex,
          length: element[1].length,
        });
        break;
      case 0:
        currentIndex += element[1].length;
        break;
      case 1:
        result.push({
          operation: DiffOperation.INSERT,
          content: element[1],
          start: currentIndex,
          length: element[1].length,
        });
        currentIndex += element[1].length;
        break;
      default:
        console.error('unexpected diff result:', element);
        break;
    }
  }
  return result;
}

exports.performDiff = performDiff;
function applyDiff(string, diffInfo) {
  var diff = __spreadArrays(diffInfo);
  var result = string;
  while (diff.length) {
    var element = diff.shift();
    switch (element.operation) {
      case DiffOperation.DELETE:
        result = result.substr(0, element.start) + result.substr(element.start + element.length);
        break;
      case DiffOperation.INSERT:
        result = result.substr(0, element.start) + element.content + result.substr(element.start);
        break;
    }
  }
  return result;
}

exports.applyDiff = applyDiff;
function revertDiff(string, diffInfo) {
  var diff = __spreadArrays(diffInfo);
  var result = string;
  while (diff.length) {
    var element = diff.pop();
    switch (element.operation) {
      case DiffOperation.INSERT:
        result = result.substr(0, element.start) + result.substr(element.start + element.length);
        break;
      case DiffOperation.DELETE:
        result = result.substr(0, element.start) + element.content + result.substr(element.start);
        break;
    }
  }
  return result;
}

exports.revertDiff = revertDiff;
//# sourceMappingURL=index.js.map
