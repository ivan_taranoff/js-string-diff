declare enum DiffOperation {
    INSERT = "insert",
    DELETE = "delete"
}
interface DiffResultElement {
    operation: DiffOperation;
    content: string;
    start: number;
    length: number;
}
declare function performDiff(newString: string, oldString: string): DiffResultElement[];
declare function applyDiff(string: string, diffInfo: DiffResultElement[]): string;
declare function revertDiff(string: string, diffInfo: DiffResultElement[]): string;
export { DiffOperation, DiffResultElement, performDiff, applyDiff, revertDiff };
